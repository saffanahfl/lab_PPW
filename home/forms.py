from django import forms

class Jadwal_Form(forms.Form) :
	hari = forms.CharField(label = 'Hari', required = True, max_length = 20, widget=forms.TextInput(attrs={'class':'form-control'}))
	tanggal = forms.DateField(label = 'Tanggal', required = True, widget=forms.DateInput(attrs = {'type' : 'date', 'class':'form-control'}))
	jam = forms.TimeField(label = 'Jam', required = True, widget = forms.TimeInput(attrs = {'type' : 'time', 'class':'form-control'}))
	kegiatan = forms.CharField(label = 'Kegiatan', required = True, max_length = 100, widget=forms.TextInput(attrs={'class':'form-control'}))
	tempat = forms.CharField(label = 'Tempat', required = True, max_length = 40, widget=forms.TextInput(attrs={'class':'form-control'}))
	kategori = forms.CharField(label = 'Kategori', required = True, max_length = 40, widget=forms.TextInput(attrs={'class':'form-control'}))