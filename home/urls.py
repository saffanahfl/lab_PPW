from django.conf.urls import url
from .views import index
from .views import kemampuan
from .views import hobi
from .views import kontak
from .views import register
from .views import jadwalkegiatan
from .views import tampiljadwal
from .views import deletejadwal
urlpatterns = [
	url(r'^$', index, name = 'index'),
	url(r'^kemampuan/', kemampuan, name = 'kemampuan'),
	url(r'^hobi/', hobi, name = 'hobi'),
	url(r'^kontak/', kontak, name = 'kontak'),
	url(r'^register/', register, name = 'register'),
	url(r'^formkegiatan/', jadwalkegiatan, name = 'jadwalkegiatan'),
	url(r'^jadwal/', tampiljadwal, name = 'tampiljadwal'),
	url(r'^hapus/', deletejadwal, name = 'deletejadwal')
]
