from django.shortcuts import render
from .forms import Jadwal_Form
from .models import JadwalPribadi
from django.http import HttpResponseRedirect

response = {'author' : 'Saffanah Fausta Lamis'}
# Create your views here.
def index(request) :
	return render(request, 'First.html')
def kemampuan(request) :
	return render(request, 'Third.html')
def hobi(request) :
	return render(request, 'Fourth.html')
def kontak(request) :
	return render(request, 'Fifth.html')
def register(request) :
	return render(request, 'Register.html')
def jadwalkegiatan(request) :
	form = Jadwal_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['hari'] = request.POST['hari']
		response['tanggal'] = request.POST['tanggal']
		response['jam'] = request.POST['jam']
		response['kegiatan'] = request.POST['kegiatan']
		response['tempat'] = request.POST['tempat']
		response['kategori'] = request.POST['kategori']
		jadwal = JadwalPribadi(hari = response['hari'], tanggal = response['tanggal'], jam = response['jam'], kegiatan = response['kegiatan'], tempat = response['tempat'], kategori = response['kategori'])
		jadwal.save()
		return HttpResponseRedirect('../formkegiatan')       
	else :
		print('input salah')
	response['jadwal_form'] = form
	html = 'Formjadwal.html'
	return render(request, html, response)
def tampiljadwal(request) :
	total = JadwalPribadi.objects.all()
	response = {'jadwals' : total }
	return render(request, 'Rekapjadwal.html', response)
def deletejadwal(request) :
	JadwalPribadi.objects.all().delete()
	return HttpResponseRedirect('../jadwal')