from django.db import models

# Create your models here.
class JadwalPribadi(models.Model):
	hari = models.CharField(max_length = 20)
	tanggal = models.DateField()
	jam = models.TimeField()
	kegiatan = models.CharField(max_length = 100)
	tempat = models.CharField(max_length = 40)
	kategori = models.CharField(max_length = 40)